# http://clarkgrubb.com/makefile-style-guide

MAKEFLAGS += --warn-undefined-variables --no-print-directory
SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c
.DEFAULT_GOAL := run
.DELETE_ON_ERROR:
.SUFFIXES:

.PHONY: run
run:
	@docker compose up -d

.PHONY: clean
clean:
	@docker compose down
	@docker compose rm -f
	@find _in -type f -exec rm {} \;
