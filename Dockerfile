FROM docker.elastic.co/logstash/logstash:7.6.0
RUN logstash-plugin install logstash-codec-cloudtrail
RUN logstash-plugin install logstash-filter-json_encode
